<%-- 
    Document   : registro
    Created on : 25-jun-2020, 18:55:42
    Author     : pablo
--%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" import="p2.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">

    <head>
        <title>Registro</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./css/estilo.css" rel="stylesheet">
        <style>
            input{
                border:1px solid #29B29D;
            }
        </style>
        <script type="text/javascript" language="javascript" src="./js/clave.js"></script>
    </head>

    <body>
        <div class="registro">
            <div class="title">
                <b>Registro</b>
                <br/>
                <hr/>
            </div>
            <form method="post" class="form" onsubmit="ProcesarForm(this, 'Registro', 'cuerpo'); return false;">
                <div>
                    <table class="login-table" style="text-align:left;">
                        <tr>
                            <td>Nombre:</td>                    
                            <td><input type="text" name="nom" required></td>
                        </tr>
                        <tr>
                            <td>Apellidos:</td>                    
                            <td><input type="text" name="ape" required></td>
                        </tr>
                        <tr>
                            <td>Usuario:</td>                    
                            <td><input type="text" name="usu" required></td>
                        </tr>
                        <tr>
                            <td>Clave:</td>
                            <td><input id="clave" type="password" name="clave" required></td>
                        </tr>
                        <tr>
                            <td>Teléfono:</td>
                            <td><input type="tel" name="telefono" required></td>
                        </tr>
                        <tr>
                            <td>Domicilio:</td>
                            <td><input type="text" name="dom" required></td>
                        </tr>
                        <tr>
                            <td>Provincia:</td>
                            <td><input type="text" name="prov" required></td>
                        </tr>
                        <tr>
                            <td>Población:</td>
                            <td><input type="text" name="pob" required></td>
                        </tr>
                        <tr>
                            <td>Código Postal:</td>
                            <td><input type="text" name="cp" required></td>
                        </tr>
                    </table>
                    <div class="login-submit">
                        <input type="submit" class="button" value="Registro"/>
                    </div>
                </div>
            </form>
        </div>
        <%  if (session.getAttribute("usuarioExiste") != null && session.getAttribute("usuarioExiste") == "true") {
                String usuario = request.getParameter("usu");
        %>
        <script>alert("El usuario <%=usuario%> ya existe.");</script>
        <%
                session.setAttribute("usuarioExiste", "false");
            }
        %>
        <%
            if (session.getAttribute("registroResult") != null && session.getAttribute("registroResult") == "false") {
        %>
        <script> alert("Registro incorrecto")</script>
        <%
                session.setAttribute("registroResult", "true");
            }
        %>
    </body>
</html>
