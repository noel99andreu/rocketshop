<%-- 
    Document   : data_pedido
    Created on : 15-may-2020, 15:13:36
    Author     : Joaquin Giner y Pablo Pons
--%>

<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" import="p2.*" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="./css/cesta.css" rel="stylesheet"> 
    </head>
    <body>
        <%
            ArrayList<Producto> pedido = (ArrayList) session.getAttribute("pedido");
        %>
        <ul> 
            <li>
                <table class="headcesta">
                    <tr> 
                        <th class="cesta-head-name"></th>
                        <th class="descr-head-name"><b>Descripcion</b></th>
                        <th class="cant-head-name"><b>cantidad</b></th>
                        <th class="precio-head-name"><b>precio</b> </th>
                    </tr>
                    <%
                        for (Producto p : pedido) {
                    %> 
                    <tr>
                        <td class="iconoCompra">
                            <img src= <%=p.getImagen()%> alt="articulo">
                        </td>
                        <td class="descr-head-name">
                            <form method="post" onclick="ProcesarForm(this, 'Articulo', 'cuerpo'); return false;">
                                <p>
                                    <input type="hidden" name="codigo" value="<%=p.getCodigo()%>">
                                    <a href="#"><%=p.getDescripcion()%></a>
                                </p>
                            </form>
                        </td>
                        <td> <%=p.getExistencias()%> </td>
                        <td class="precio-head-name"><%=p.getPrecio()%>€</td>
                    </tr>
                    <%
                        }
                    %>

                </table>
            </li>
            <hr/>
        </ul>
    </body>
</html>
