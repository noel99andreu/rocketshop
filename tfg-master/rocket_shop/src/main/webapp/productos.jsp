<%@page import="java.util.List"%>
<%@page contentType="text/html" import="p2.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>&nbsp;</title>
        <script src="./js/libCapas.js"></script>
        <script src="./js/carrito.js"></script>
    </head>
    <body>
        <h1><p class="fbien">Listado de Productos</p></h1>
        <%
            AccesoBD con = AccesoBD.getInstance();
            List<ProductoBD> productos = con.obtenerProductosBD();
        %>
        <div>
            <table>
                <tr>
                    <th>Código</th>
                    <th></th>
                    <th>Descripción</th>
                    <th>&nbsp;</th>
                    <th>Precio</th>
                    <th>Disponibles</th>
                </tr>
                <%
                    for (ProductoBD producto : productos) {
                        int codigo = producto.getId();
                        String descripcion = producto.getDescripcion();
                        float precio = producto.getPrecio();
                        int existencias = producto.getStock();
                        String imagen = producto.getImagen();
                %>
                <tr>
                    <td><%=codigo%></td>
                    <td><img src="<%=imagen%>" alt="<%=descripcion%>"></td>
                    <td><%=descripcion%></td>
                    <td>&nbsp;</td>
                    <td><%=precio%>M €</td>
                    <td><%=existencias%></td>
                    <td>
                        <%
                            if (existencias > 0) {
                        %>
                        <input type="button" id=<%=codigo%> value="Añadir al carrito" onclick="AnyadirCarritoStorage(<%=codigo%>, '<%=descripcion%>',<%=precio%>,<%=existencias%>, 1);Cargar('carrito.jsp', 'cuerpo')">
                        <%
                        } else {
                        %>
                        <h1><p class="fbien">No hay productos disponibles.</p></h1>
                        <input type="button" value="Finalizar" onclick='Cargar("inicio.html", "cuerpo")'>
                        <%
                            }
                        %>
                    </td>
                </tr>
                <%
                    }
                %>
            </table>
        </div>
    </body>
</html>
