<%-- 
    Document   : login
    Created on : 26-may-2020, 12:13:55
    Author     : Joaquin Giner y Pablo Pons
--%>

<%@page contentType="text/html" import="p2.*" import="java.util.ArrayList" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>&nbps;</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./css/estilo.css" rel="stylesheet">

    </head>
    <body>
        <%
            if (session.getAttribute("usuario") == null) {
        %>
        <script> Cargar('login.jsp', 'cuerpo');</script>
        <%
        } else {
        %>
        <h1><p class="fbien">Portal del Usuario</p></h1>
        <div>


            <form method="post" onsubmit="ProcesarForm(this, 'CerrarSesion', 'cuerpo'); return false;">
                <input class="button" type="submit" value="Cerrar sesión">
            </form>
            <div>
                <button class="button" onclick="Cargar('config_data_user.jsp', 'cuerpo')">
                    Dirección y datos personales
                </button>
            </div>
            <div>
                <button class="button" id="pedidos" onclick="Cargar('modificacionpedido.jsp', 'cuerpo')">
                    Pedidos
                </button>
            </div>   
        </div>

        <%
            }
        %>
    </body>
</html>
