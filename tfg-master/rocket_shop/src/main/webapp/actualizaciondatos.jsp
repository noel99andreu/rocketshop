<%-- 
    Document   : actualizaciondatos
    Created on : 28-jun-2020, 11:18:03
    Author     : pablo
--%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" import="p2.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Actualización de Datos</title>
    </head>
    <body>
        <%
                                String user = session.getAttribute("usuario").toString();
                                
                                AccesoBD con = new AccesoBD(); // Creamos un objeto para acceder a la BBDD
                                int codigoU = con.getCodigoUsuario(user);
                                Usuario usu = new Usuario();
                                usu = con.obtenerDatosUsuario(codigoU);
                                
                                String usuario = usu.getUsuario();
                                String nombre = usu.getNombre();
                                String clave = usu.getClave();
                                String apellidos = usu.getApellido();
                                String direccion = usu.getDireccion();
                                String localidad = usu.getLocalidad();
                                String provincia = usu.getProvincia();
                                String telefono = usu.getTelefono();
                                String cp = usu.getCp();
                            %>
                            
        <form method="post" class="form" onsubmit="ProcesarForm(this, 'ActualizarDatos', 'cuerpo'); return false;">
            <input type="hidden" name="update" value="update_data">
            <table class="table-edit" style="text-align:left;">
                <tr>
                    <td>Usuario:</td>
                <input type="hidden" name="usu" value="<%=usuario%>">
                <td><%=usuario%></td>
                </tr>
                <tr>
                    <td>Nombre:</td>                    
                    <td><input type="text" name="nom" value="<%=nombre%>" required></td>
                </tr>
                <tr>
                    <td>Apellidos:</td>                    
                    <td><input type="text" name="ape" value="<%=apellidos%>" required></td>
                </tr>
                <tr>
                    <td>Teléfono:</td>
                    <td><input type="tel" name="telefono" value="<%=telefono%>" required></td>
                </tr>
                <tr>
                    <td>Domicilio:</td>
                    <td><input type="text" name="dom" value="<%=direccion%>" required></td>
                </tr>
                <tr>
                    <td>Provincia:</td>
                    <td><input type="text" name="prov" value="<%=provincia%>" required></td>
                </tr>
                <tr>
                    <td>Población:</td>
                    <td><input type="text" name="pob" value="<%=localidad%>" required></td>
                </tr>
                <tr>
                    <td>Código Postal:</td>
                    <td><input type="text" name="cp" value="<%=cp%>" required></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" class="button" value="Guardar"> </td>
                </tr>
            </table>
        </form>
        <button class="button" onclick='Cargar("actualizacionpasswd.jsp", "cuerpo")'>Cambiar Contraseña</button>
    </body>
</html>
