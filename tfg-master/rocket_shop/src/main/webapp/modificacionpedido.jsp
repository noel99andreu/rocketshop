<%-- 
    Document   : modificacionpedido
    Created on : 30-dic-2020, 22:03:13
    Author     : PABLO_2
--%>

<%@page import="java.util.List"%>
<%@page import="java.sql.Date"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" import="p2.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Modificación de Pedido</title>
    </head>
    <body>
        <%
            if (session.getAttribute("usuario") != null) {

                String user = session.getAttribute("usuario").toString();

                AccesoBD con = new AccesoBD(); // Creamos un objeto para acceder a la BBDD
                int codigo = con.getCodigoUsuario(user);
                List<Pedido> pedidos = con.obtenerPedido(codigo);

                int codigoPedido;
                float precio;
                Date fecha;
                String estado;
                for (Pedido pedido : pedidos) {
                    codigoPedido = pedido.getCodigo();
                    precio = pedido.getPrecioTotal();
                    fecha = pedido.getFecha();
                    estado = pedido.getEstado();
        %>
        <form method="POST" onsubmit="ProcesarForm(this, 'CancelarPedido', 'cuerpo');
                return false;">
            <table class="table-edit" style="text-align:left;">
                <tr>
                    <td>Código de Pedido:</td>
                    <td><%=codigoPedido%></td>
                    <td><input type="text" value="<%=codigoPedido%>" name="codigoPedido" hidden></td>
                </tr>
                <tr>
                    <td>Fecha:</td>                    
                    <td><%=fecha%></td>
                </tr>
                <tr>
                    <td>Importe:</td>
                    <td><%=precio%></td>
                </tr>
                <tr>
                    <td>Estado:</td>
                    <td><%=estado%></td>
                </tr>
                <tr>
                    <td><input type="submit" value="Eliminar"></td>
                </tr>
            </table>  
        </form>
        
        <% }%>

        <% } else {%>
        <h1><p class="fbien">No se ha iniciado sesión</p></h1>
        <input type="button" value="Iniciar sesión" onclick='Cargar("user_menu.jsp", "cuerpo")'>
        <% }%>
        <input type="button" value="Volver" onclick='Cargar("user_menu.jsp", "cuerpo")'>
    </body>
</html>

