<%-- 
    Document   : pedidoFinalizado
    Created on : 22-dic-2020, 23:44:11
    Author     : PABLO_2
--%>

<%@page language="java" import="p2.*" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="./css/estiloFinalizado.css" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="./css/estilo.css" rel="stylesheet">
        <title>Finalizar Pedido</title>
        <script>
                localStorage.clear();
                delete carrito; 
        </script>
           
    </head>
    <body>
        <% if((session.getAttribute("carritoProcesado") != null)) { System.out.println("El total es: " + request.getParameter("total")); %>
            
        <h1><p class="fbien">Pedido realizado de forma satisfactoria.</p></h1>
        <input type="button" value="Finalizar" onclick='Cargar("inicio.html", "cuerpo")'>
        <% } else { %>
        <h1><p class="fbien">No se ha realizado ningún pedido.</p></h1>
        <input type="button" value="Finalizar" onclick='Cargar("inicio.html", "cuerpo")'>
        <% } %>
    </body>
</html>

