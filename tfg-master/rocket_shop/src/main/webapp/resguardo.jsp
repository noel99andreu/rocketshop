<%-- 
    Document   : resguardo
    Created on : 16-dic-2020, 0:25:17
    Author     : PABLO_2
--%>

<%@page import="java.util.List"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.LinkedList"%>
<%@page language="java" import="p2.*" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resguardo</title>
        <link href="./css/estilo.css" rel="stylesheet">
        <script type="text/javascript" language="javascript" src="./js/pago.js"></script>
    </head>
    <body>

        <%
            if (session.getAttribute("usuario") != null) {
                AccesoBD con = AccesoBD.getInstance();
                int codigo = con.getCodigoUsuario((String) session.getAttribute("usuario"));
                Usuario usuario = con.obtenerDatosUsuario(codigo);
                List<Producto> producto = (List<Producto>) session.getAttribute("carritoProcesado");

        %>
        <h1><p class="fbien">Resguardo</p></h1>
        <form method="POST" onsubmit="ProcesarForm(this, 'Tramitacion', 'cuerpo');
                return false;">
            <table class="login-table" style="text-align:left;">
                <tr>
                    <td><input type="text" value="<%=codigo%>" name="codigo" hidden></td>
                </tr>
                <tr>
                    <td>Nombre:</td>
                    <% if (usuario.getNombre() != null) {%>
                    <td><input type="text" value="<%=usuario.getNombre()%>" name="nombre" required></td>
                        <%} else {%>
                    <td><input type="text" name="nombre" required></td>
                        <%}%>
                </tr>
                <tr>
                    <td>Apellidos:</td>
                    <% if (usuario.getApellido() != null) {%>
                    <td><input type="text" value="<%=usuario.getApellido()%>" name="apellidos" required></td>
                        <%} else {%>
                    <td><input type="text" name="apellidos" required></td>
                        <%}%>
                </tr>
                <tr>
                    <td>Domicilio:</td>
                    <% if (usuario.getDireccion() != null) {%>
                    <td><input type="text" value="<%=usuario.getDireccion()%>" name="domicilio" required></td>
                        <%} else {%>
                    <td><input type="text" name="domicilio" required></td>
                        <%}%>
                </tr>
                <tr>
                    <td>Población:</td>
                    <% if (usuario.getLocalidad() != null) {%>
                    <td><input type="text" value="<%=usuario.getLocalidad()%>" name="poblacion" required></td>
                        <%} else {%>
                    <td><input type="text" name="poblacion" required></td>
                        <%}%>
                </tr>
                <tr>
                    <td>Provincia:</td>
                    <% if (usuario.getProvincia() != null) {%>
                    <td><input type="text" value="<%=usuario.getProvincia()%>" name="provincia" required></td>
                        <%} else {%>
                    <td><input type="text" name="provincia" required></td>
                        <%}%>
                </tr>
                <tr>
                    <td>Código Postal:</td>
                    <% if (usuario.getCp() != null) {%>
                    <td><input type="num" value="<%=usuario.getCp()%>" name="cp" required></td>
                        <%} else {%>
                    <td><input type="num" name="cp" required></td>
                        <%}%>
                </tr>
                <tr>
                    <td>Teléfono:</td>
                    <% if (usuario.getTelefono() != null) {%>
                    <td><input type="num" value="<%=usuario.getTelefono()%>" name="telefono" required></td>
                        <%} else {%>
                    <td><input type="num" name="telefono" required></td>
                        <%}%>
                </tr>
                <tr>
                    <td><input type="radio" id="tar" name="pago" onclick="myFunction()">Tarjeta de Crédito</td>

                    <td><p id="texttar" style="display:none"><input type="text" name="tar" value="0" required></p></td>
                </tr>
                <tr>
                    <td><input type="radio" id="transf" name="pago" onclick="myFunction()">Transferencia</td>

                    <td><p id="texttransf" style="display:none"><input type="text" name="transf" value="0" required></p></td>
                </tr>

                    <%  float total = 0, precioUnidad = 0;
                        int nUnidades = 0;
                        String nombreProducto;
                        for (int i = 0; i < producto.size(); i++) {
                            precioUnidad = producto.get(i).getPrecio();
                            nUnidades = producto.get(i).getCantidad();
                            total += precioUnidad * nUnidades;
                            nombreProducto = producto.get(i).getDescripcion();

                    %>
                <tr>
                    <td>Nombre del Producto:</td>
                    <td><p class="f1"><%=nombreProducto%></p></td>
                </tr>
                <tr>
                    <td>Precio por Unidad:</td>
                    <td><p class="f1"><%=precioUnidad%>M €</p><input type="hidden" value="<%=precioUnidad%>" name="pu" readonly="readonly" required></td>
                </tr>
                <tr>
                    <td>Número de Unidades:</td>
                    <td><p class="f1"><%=nUnidades%></p><input type="hidden" value="<%=nUnidades%>" name="uni" readonly="readonly" required></td>
                </tr>
                <%  }%>
                <tr>
                    <td>Total:</td>
                    <td><p class="fbien"><%=total%>M €</p><input type="hidden" value="<%=total%>" name="total" readonly="readonly" required></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="Tramitar Pedido"></td>
                </tr>
            </table>
        </form>
        <% } else { %>
        <h1><p class="fbien">No se ha iniciado sesión</p></h1>
        <input type="button" value="Iniciar sesión" onclick='Cargar("user_menu.jsp", "cuerpo")'>
        <% }%>

    </body>
</html>
