<%-- 
    Document   : actualizacionpasswd
    Created on : 28-jun-2020, 16:27:20
    Author     : pablo
--%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" import="p2.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cambiar Contraseña</title>
    </head>
    <body>
        <% String user = session.getAttribute("usuario").toString();
                                
                                AccesoBD con = new AccesoBD(); // Creamos un objeto para acceder a la BBDD
                                int codigoU = con.getCodigoUsuario(user);
                                Usuario usu = new Usuario();
                                usu = con.obtenerDatosUsuario(codigoU);
                                
                                String usuario = usu.getUsuario();
         %>
        <form method="post" class="form" onsubmit="ProcesarForm(this, 'ActualizarDatos', 'cuerpo'); return false;"> 
            <input type="hidden" name="update" value="update_pass">
            <table class="table-edit" style="text-align:left;">
                <input type="hidden" name="usu" value="<%=usuario%>">
                <tr>
                    <td>Nueva Contraseña:</td>
                    <td><input type="password" name="pwd" required></td>
                </tr>
                <tr>
                    <td>Confirmar contraseña:</td>
                    <td><input type="password" name="pwd2" required></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" class="button" value="Guardar"> </td>
                </tr>
            </table>
        </form>

    </body>
</html>
