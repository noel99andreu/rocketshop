<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" import="p2.*" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./css/config-user.css" rel="stylesheet" type="text/css">
        <link href="./css/estilo.css" rel="stylesheet">
        <title>JSP Page</title>
    </head>

    <body>

        <%
            String user = session.getAttribute("usuario").toString();

            AccesoBD con = new AccesoBD(); // Creamos un objeto para acceder a la BBDD
            int codigoU = con.getCodigoUsuario(user);
            Usuario usu = new Usuario();
            usu = con.obtenerDatosUsuario(codigoU);

            String usuario = usu.getUsuario();
            String nombre = usu.getNombre();
            String apellidos = usu.getApellido();
            String direccion = usu.getDireccion();
            String localidad = usu.getLocalidad();
            String provincia = usu.getProvincia();
            String telefono = usu.getTelefono();
            String cp = usu.getCp();
        %>
        
        <table style="text-align:left;">
            <tr>
                <th></th><th></th>
            </tr>
            <tr>
                <td>Usuario:</td>&nbsp;<td><%=usuario%></td>
            </tr>
            <tr>
                <td>Nombre:</td>&nbsp;<td><%=nombre%> <%=apellidos%></td>
            </tr>
            <tr>
                <td>Dirección:</td>&nbsp;<td><%=direccion%>, <%=provincia%></td>
            </tr>
            <tr>
                <td>Localidad:</td>&nbsp;<td><%=localidad%>, <%=cp%></td>
            </tr>
            <tr>
                <td>Móvil: </td>&nbsp;<td> <%=telefono%></td>
            </tr>
        </table>
        <br/>
        <button class="button" onclick='Cargar("actualizaciondatos.jsp", "cuerpo")'>Editar</button>
    </body>
</html>
