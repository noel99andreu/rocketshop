<%-- 
        Document   : carrito
        Created on : 13-may-2020, 10:14:36
        Author     : asus
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" import="p2.*" import="java.util.ArrayList" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>&nbps;</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./css/cesta.css" rel="stylesheet"> 
    </head>

    <body>
        <div class="divespacio"></div>
        <ul>
            <li>
                <table class="headcesta">
                    <tr> 
                        <td class="head-name"> <b>1- Dirección de envío:</b></td>
                        <td class="descr-head-name"> 
                            <%
                                String user = session.getAttribute("usuario").toString();
                                AccesoBD con = new AccesoBD(); // Creamos un objeto para acceder a la BBDD
                                int id = con.getCodigoUsuario(user);
                                Usuario usuario = con.obtenerDatosUsuario(id);
                                String direccion = usuario.getDireccion();
                                String localidad = usuario.getLocalidad();
                                String provincia = usuario.getProvincia();
                                String cp = usuario.getCp();
                            %>

                            <br/>
                            <%=direccion%>
                            <br/>
                            <%=localidad%>, <%=provincia%> <%=cp%>
                    </tr>
                </table>    
            </li>
            <br/>
            <hr/>
            <br/>
            <li>
                <table class="headcesta">
                    <tr> 
                        <td class="head-name"> <b>1- Método de pago</b></td>
                        <td class="descr-head-name"> Número de tarjeta: 100000
                            <br/>
                            Dirección de facturación: Igual a la dirección de envío.
                        </td>
                    </tr>
                </table>			
            </li>
            <br/>
            <br/>
            <hr/>

            <li>
                <table class="headcesta">
                    <tr> 
                        <th class="cesta-head-name"></th>
                        <th class="descr-head-name"><b>Descripcion</b></th>
                        <th class="cant-head-name"><b>cantidad</b></th>
                        <th class="precio-head-name"><b>precio</b> </th>
                    </tr>


                    <%
                        ArrayList<Producto> carc = (ArrayList) session.getAttribute("carrito");
                        Producto p = null;
                        int precioTotal = 0;
                        if (carc != null) { // Si e carrito no está vacío
                            for (int i = 0; i < carc.size(); i++) { // Para todos los elementos del carrito
                                p = carc.get(i); // Cogemos el elemento actual
                                precioTotal += p.getPrecio() * p.getExistencias();
                    %>


                    <tr>
                        <td class="iconoCompra">
                            <img src= <%=p.getImagen()%> alt="articulo">
                        </td>
                        <td class="descr-head-name">
                            <form method="post" onclick="ProcesarForm(this, 'Articulo', 'cuerpo'); return false;">
                                <p>
                                    <input type="hidden" name="codigo" value="<%=p.getCodigo()%>">
                                    <a href="#"><%=p.getDescripcion()%></a>
                                </p>
                            </form>
                        </td>
                        <td> <%=p.getExistencias()%> </td>
                        <td class="precio-head-name"><%=p.getPrecio()%>M €</td>
                    </tr>	
                    <%
                            }
                        }
                    %>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>Precio Total: </td>
                        <td class="precio-head-name"><%=precioTotal%>€</td>
                    </tr>	
                </table>
            </li>
            <hr/>


            <form method="post" onclick="ProcesarForm(this, 'FinalizarPedido', 'cuerpo'); return false;">
                <p>
                    <input type="button" value="Cancelar pedido" onclick='Cargar("carrito.jsp", "cuerpo")'>
                    <input type="hidden" name="precioTotal" value="<%=precioTotal%>">
                    <input type="submit" value="Finalizar pedido">
                </p>
            </form>


    </body>
</html>

