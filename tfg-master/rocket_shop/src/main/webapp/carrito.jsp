<%-- 
    Document   : carrito
    Created on : 14-may-2020, 10:15:45
    Author     : Joaquin Giner y Pablo Pons
--%>

<%@page contentType="text/html" import="p2.*" import="java.util.ArrayList" import="com.google.gson.Gson" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>&nbps;</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./css/cesta.css" rel="stylesheet">  
        <script type="text/javascript" language="javascript" src="./js/libCapas.js"></script>
        <script type="text/javascript" language="javascript" src="./js/carrito.js"></script>
    </head>
    <h1><p class="fbien">Carrito</p></h1>
    <body>  
        <table>
            <tbody id="mostrar_carrito">
                <tr>
                    <script>
                        document.getElementById("mostrar_carrito").innerHTML=CargarCarritoStorage();
                        document.getElementById("mostrar_carrito").innerHTML=MostrarCarritoStorage();
                    </script>
                </tr>
            </tbody>
        </table>
            <p>
                <input type="button" value="Volver a productos" onclick='Cargar("productos.jsp", "cuerpo")'>
                <input type="button" value="Formalizar pedido" onclick='ProcesarCarrito("carrito", "ProcesarPedido", "cuerpo")'>
                <input type="button" value="Eliminar Carrito" onclick='BorrarCarrito();Cargar("carrito.jsp", "cuerpo")'>
            </p> 
    </body>
</html>
