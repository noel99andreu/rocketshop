<%-- 
    Document   : login
    Created on : 20-may-2020, 21:40:24
    Author     : Joaquin Giner y Pablo Pons
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">

    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./css/login.css" rel="stylesheet">
        <link href="./css/estilo.css" rel="stylesheet" type="text/css">
    </head>
    <%
        //Utilizamos una variable en la sesión para informar de los mensajes de Error           
        String mensaje = (String) session.getAttribute("mensaje");
        if (mensaje != null) { //Eliminamos el mensaje consumido     
            session.removeAttribute("mensaje");
    %>
    <h1> <%=mensaje%> </h1>
    <% }
        //Se obtiene el usuario actual registrado en el servicio web, del entorno de sesión          
        String usuarioActual = (String) session.getAttribute("usu");
        if (usuarioActual == null) //No hay usuario registrado          
        {
            //Mostramos el formulario para la introducción del usuario y la clave          
    %>
    <h1><p class="fbien">Bienvenido</p></h1>
    <form method="post" onsubmit="ProcesarForm(this, 'AccesoLogin', 'cuerpo');return false" >
        <table class="login-table">
            <tr>
              <td>Usuario:</td><td><input name="usu" type="text" required/></td>
            </tr>
            <tr>
                <td>Contrase&ntilde;a:</td><td><input name="clave" type="password" required/></td>
            </tr>
            <br/>
        </table>
        <br/><br/>
        <input type="radio" name="tipoAcceso" value="Acceso" checked="checked"/> Acceso 
        <input type="radio" name="tipoAcceso" value="Registro" onclick="Cargar('registro.jsp', 'cuerpo')"/> Registro
        <br/><br/>
        <input type="submit" value="Entrar a la Tienda Virtual"onclick="Cargar('user_menu.jsp', 'cuerpo')"/>
        <br/>

    </form>          
    <%
    } else {
        response.sendRedirect("user_menu.jsp");          
        }
    %> 
 <%
        if ((session.getAttribute("registroResult") != null) && session.getAttribute("registroResult") == "true") {
            String usuario = request.getParameter("usu");
    %>
    <script>alert("Usuario <%=usuario%> registrado correctamente.");</script>
    <%
        session.removeAttribute("registroResult");
        }
    %>   
    <%
        if ((session.getAttribute("loginResult") != null) && session.getAttribute("loginResult") == "true") {
    %>
    <script>alert("Login incorrecto");</script>
    <%
            session.setAttribute("loginResult", "false");
        }
    %>   
 
</body>
</html>
