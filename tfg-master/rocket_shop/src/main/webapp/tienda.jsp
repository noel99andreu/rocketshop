<%-- 
    Document   : tienda.jsp
    Created on : 26-may-2020, 12:13:55
    Author     : Joaquin Giner y Pablo Pons
--%>

<%@page contentType="text/html" import="p2.*" import="java.util.ArrayList" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>&nbps;</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./css/login.css" rel="stylesheet">
    </head>
    <body>
        <%

            if (session.getAttribute("usuario") == null) {
        %>
        <script>Cargar('login.jsp', 'cuerpo');</script>
        <%
        } else {
        %>
        <script>Cargar('resguardo.jsp', 'cuerpo');</script>
        <%            }
        %>
    </body>
</html>
