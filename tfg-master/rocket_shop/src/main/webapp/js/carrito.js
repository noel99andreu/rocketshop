/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Javascript
var carrito = null; 
    
function CargarCarritoStorage() {
    carrito=JSON.parse(localStorage.getItem("carrito")); 
    if(carrito == null){
        carrito = new Object();
    }
}

function GuardarCarritoStorage() {
   localStorage.setItem("carrito",JSON.stringify(carrito));
}

function AnyadirCarritoStorage(codigo, descripcion, precio, existencias, cantidad){
    CargarCarritoStorage();
    var c;
    var existe = 0;

    for(c in carrito){
        if(c == codigo){
            existe = 1;
            carrito[c].cantidad ++;
            break;
        }
    }
    if(existe == 0){
        var producto = {descripcion:descripcion, cantidad:cantidad, existencias:existencias, precio:precio};
        carrito[codigo] = producto;
    }
    GuardarCarritoStorage();
}

function IncrementarCantidad(codigo){ 
   CargarCarritoStorage();  
   for(c in carrito) {
       if (c == codigo && carrito[c].cantidad < carrito[c].existencias) {
            AnyadirCarritoStorage(codigo, carrito[c].descripcion, carrito[c].precio, carrito[c].existencias, 1);
            break;
        }
    }
}

function DisminuirCantidad(codigo){ 
   CargarCarritoStorage();  
   var c;
   for(c in carrito) {
       if (c == codigo) {
           if (carrito[c].cantidad == 1) {
               delete carrito[c];
               break;
           }
           else{
                carrito[c].cantidad--;
                break;
           }
       }
    }
    GuardarCarritoStorage();
}

function BorrarCarrito(){ 
  CargarCarritoStorage(); 
   var c;
   for(c in carrito){  
       delete carrito[c];
   }
   GuardarCarritoStorage();
}

function MostrarCarritoStorage(){

    var c, texto = "<tr><th></th><th>Descripción</th><th>Unidades</th><th>Precio</th><th>Total</th></tr>";
    var total = 0;
    for(c in carrito){
        texto+="<tr>";
        texto += "<td><img src='./img/box_pedido.png' alt='imagen'></td><td>"+carrito[c].descripcion+"</td><td style='width:50%'><input type='button' value='-' onclick=DisminuirCantidad("+[c]+");Cargar('carrito.jsp','cuerpo')>&nbsp;"+carrito[c].cantidad+"&nbsp;<input type='button' value='+' onclick=IncrementarCantidad("+[c]+");Cargar('carrito.jsp','cuerpo')></td><td style='width:30%'>"+carrito[c].precio+"</td><td style='width:30%'>"+carrito[c].cantidad*carrito[c].precio+" M €</td><br/>";
        texto+="</tr>";
        total += carrito[c].cantidad*carrito[c].precio;
    }
    texto += "Total: "+total+" M €";
    return texto;
}

function ProcesarCarrito(carrito, url, capa)
{
    carritoParse = JSON.parse(localStorage.getItem(carrito));
    var valores = "carrito=" + JSON.stringify(carritoParse);
    CargarForm(url, capa, valores);
}
