/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p2;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Random;
/**
 *
 * @author Joaquin Giner y Pablo Pons
 */
public class Registro extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String usu, nom, ape, telefono, pob, prov, dom, clave, cp;
        String act = "1";
        

        nom = request.getParameter("nom");
        ape = request.getParameter("ape");
        usu = request.getParameter("usu");
        telefono = request.getParameter("telefono");
        prov = request.getParameter("prov");
        pob = request.getParameter("pob");
        dom = request.getParameter("dom");
        clave = request.getParameter("clave");
        cp = request.getParameter("cp");
            
        AccesoBD con = new AccesoBD();
        if(con.comprobarUsuarioExistenteBD(usu)){
            HttpSession sesion = request.getSession(true);
            sesion.setAttribute("usuarioExiste", "true");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/registro.jsp");
            dispatcher.forward(request, response);
        }
        else{
            if(con.registrarUsuarioBD(usu, nom, act, ape, telefono, prov, dom, clave, cp, pob)){
                HttpSession sesion = request.getSession(true);
                sesion.setAttribute("registroResult", "true");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
                dispatcher.forward(request, response);
            }
            else{
                HttpSession sesion = request.getSession(true);
                sesion.setAttribute("registroResult", "false");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/registro.jsp");
                dispatcher.forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
