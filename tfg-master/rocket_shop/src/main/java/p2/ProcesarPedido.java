/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p2;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParser.Event;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Objects;

/**
 *
 * @author PABLO_2
 */
@WebServlet(name = "ProcesarPedido", urlPatterns = {"/ProcesarPedido"})
public class ProcesarPedido extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<Producto> carrito = new ArrayList();
        AccesoBD con = AccesoBD.getInstance();
        String carritoJSON = request.getParameter("carrito");
        if (!carritoJSON.toString().equals("{}") && !carritoJSON.toString().equals("null")) {
            System.out.println("El carrito es: " + carritoJSON + " es decir, no es null");
            String indice;
            JsonReader jsonReader = Json.createReader(new StringReader(carritoJSON));
            JsonObject jobj = jsonReader.readObject();
            if (jobj.size() > 0) {
                JsonParser jsonParser = Json.createParser(new StringReader(carritoJSON));
                while (jsonParser.hasNext()) {
                    Event e = jsonParser.next();
                    if (e == Event.KEY_NAME) {
                        indice = jsonParser.getString();
                        if (!indice.equals("descripcion") && !indice.equals("cantidad") && !indice.equals("existencias") && !indice.equals("precio")) {
                            int stock = con.getStock(Integer.parseInt(indice));
                            Producto nuevo = new Producto();
                            JsonObject prod = jobj.getJsonObject(indice);
                            nuevo.setCodigo(Integer.parseInt(indice));
                            nuevo.setDescripcion(prod.get("descripcion").toString());
                            nuevo.setPrecio(Float.parseFloat(prod.get("precio").toString()));
                            if (Integer.parseInt(prod.get("cantidad").toString()) <= stock) {
                                nuevo.setCantidad(Integer.parseInt(prod.get("cantidad").toString()));
                            } else {
                                nuevo.setCantidad(stock);
                            }
                            carrito.add(nuevo);
                        }
                    }
                }
            }
            HttpSession sesion = request.getSession(true);
            sesion.setAttribute("carritoProcesado", carrito);
            request.getRequestDispatcher("/resguardo.jsp").forward(request, response);
        } else {
            System.out.println("El carrito es: " + carritoJSON + " es decir, es null");
            request.getRequestDispatcher("/pedidosvacio.jsp").forward(request, response);

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
