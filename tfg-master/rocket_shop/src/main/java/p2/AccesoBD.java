package p2;

import java.sql.*;
import java.sql.Date;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;

public class AccesoBD {

    private static AccesoBD instanciaUnica = null;
    private Connection conexionBD = null;

    public static AccesoBD getInstance() {
        if (instanciaUnica == null) {
            instanciaUnica = new AccesoBD();
        }
        return instanciaUnica;
    }

    public AccesoBD() {
        abrirConexionBD();
    }

    public void abrirConexionBD() {
        if (conexionBD == null) { // tvbbdd es el nombre de la base de datos que hemos creado con anterioridad.
            String nombreConexionBD = "jdbc:mysql://:3306/rocket_p2?verifyServerCertificate=false&useSSL=false&requireSSL=false";
            try { // root y sin clave es el usuario por defecto que crea XAMPP.
                Class.forName("com.mysql.cj.jdbc.Driver");
                conexionBD = DriverManager.getConnection(nombreConexionBD, "root", "Eafa3caz95!");
            } catch (Exception e) {
                System.out.println("No se ha podido conectar a la BB.DD...");
            }
        }
    }

    public boolean comprobarAcceso() {
        abrirConexionBD();
        return conexionBD != null;
    }

    public void cerrarConexionBD() {
        if (conexionBD != null) {
            try {
                conexionBD.close();
                conexionBD = null;
            } catch (Exception e) {
                //Error en la conexión con la BD
                System.out.println("No se ha completado la desconexión ...");
            }
        }
    }

    public List<ProductoBD> obtenerProductosBD() {
        abrirConexionBD();
        ArrayList<ProductoBD> productos = new ArrayList<>();
        ProductoBD producto;
        ResultSet resultados = null;
        try {
            String con;
            Statement s = conexionBD.createStatement();
            con = "SELECT codigo,descripcion,precio,existencias,imagen FROM productos";
// hay que tener en cuenta las columnas de vuestra tabla de productos
// también se puede utilizar una consulta del tipo:
// con = "SELECT * FROM productos";
            resultados = s.executeQuery(con);
            while (resultados.next()) {
                producto = new ProductoBD();
                producto.setId(resultados.getInt("codigo"));
                producto.setDescripcion(resultados.getString("descripcion"));
                producto.setPrecio(resultados.getFloat("precio"));
                producto.setStock(resultados.getInt("existencias"));
                producto.setImagen(resultados.getString("imagen"));
                productos.add(producto);
            }
        } catch (Exception e) {
            System.out.println("Error ejecutando la consulta a la BB.DD....");
        }
        return productos;
    }

    public int obtenerCantidadProducto(int codigo) {
        int codigoProducto = 0;
        try {
            Statement s = conexionBD.createStatement();
            String query = "SELECT existencias FROM productos WHERE codigo='" + codigo + "'";
            ResultSet resultados = s.executeQuery(query);
            resultados.next();
            codigoProducto = resultados.getInt("existencias");
            System.out.println(codigoProducto);
            return codigoProducto;
        } catch (Exception ex) {
            ex.getMessage();
            System.out.println("Error obteniendo la cantidad del producto solicitado.");
        }

        return codigoProducto;

    }

    public boolean registrarUsuarioBD(String usu, String nom, String act, String ape, String telefono, String prov, String dom, String clave, String cp, String pob) {
        boolean estado = false;
        PreparedStatement s;
        try {
            String con;
            abrirConexionBD();
            con = "INSERT INTO usuarios (usuario, nombre,apellidos,activo,telefono,provincia,domicilio,clave,cp,poblacion) values(?,?,?,?,?,?,?,?,?,?)";
            s = conexionBD.prepareStatement(con);
            s.setString(1, usu);
            s.setString(2, nom);
            s.setString(3, ape);
            s.setString(4, act);
            s.setString(5, telefono);
            s.setString(6, prov);
            s.setString(7, dom);
            s.setString(8, clave);
            s.setString(9, cp);
            s.setString(10, pob);
            if (s.executeUpdate() == 1) {
                estado = true;
            } else {
                estado = false;
            }
        } catch (Exception e) {
            estado = false;
            System.out.println(e.getMessage());
            System.out.println("Error introduciendo el usuario en la BB.DD....");
        }
        return estado;
    }

    public boolean comprobarUsuarioBD(String usuario, String clave) {
        abrirConexionBD();
        ResultSet resultados = null;
        try {
            String con;
            Statement s = conexionBD.createStatement();
//Consulta, buscamos una correspondencia usuario/clave
            con = "SELECT * FROM usuarios WHERE usuario='" + usuario + "' and clave='" + clave + "'";
            resultados = s.executeQuery(con);
            if (resultados.next()) //El usuario/clave se encuentra en la BD
            {
                return true;
            } else //El usuario/clave no se encuentra en la BD
            {
                return false;
            }
        } catch (Exception e) {
//Error en la conexión con la BD
            System.out.println("No se ha completado la peticion...");
            return false;
        }
    }

    public boolean comprobarUsuarioExistenteBD(String usu) {
        boolean res = false;
        abrirConexionBD();
        ResultSet resultados = null;
        try {
            String con;
            Statement s = conexionBD.createStatement();
            con = "SELECT usuario FROM usuarios WHERE usuario='" + usu + "'";
// hay que tener en cuenta las columnas de vuestra tabla de productos
// también se puede utilizar una consulta del tipo:
// con = "SELECT * FROM productos";
            resultados = s.executeQuery(con);
            if (resultados.next()) {
                res = usu.equals(resultados.getString("usuario"));
                System.out.println("El usuario existe.");
            } else {
                res = false;
                System.out.println("El usuario no existe.");
            }
            cerrarConexionBD();
        } catch (Exception e) {
            System.out.println("Error comprobando usuario....");
        }

        return res;
    }

    public boolean comprobarClaveBD(String usu, String clave) {
        boolean res = false;
        abrirConexionBD();
        ResultSet resultados = null;
        try {
            String con;
            Statement s = conexionBD.createStatement();
            con = "SELECT clave FROM usuarios WHERE usuario='" + usu + "'";
// hay que tener en cuenta las columnas de vuestra tabla de productos
// también se puede utilizar una consulta del tipo:
// con = "SELECT * FROM productos";
            resultados = s.executeQuery(con);
            if (resultados.next()) {
                res = clave.equals(resultados.getString("clave"));
            } else {
                res = false;
            }
            cerrarConexionBD();
        } catch (Exception e) {
            System.out.println("Error comprobando contraseña....");
        }

        return res;
    }

    public int getCodigoUsuario(String usuario) {
        abrirConexionBD();
        ResultSet resultados;
        int codigo = 0;
        try {
            String con;
            Statement s = conexionBD.createStatement();
            con = "SELECT codigo FROM usuarios WHERE usuario='" + usuario + "'";
            resultados = s.executeQuery(con);

            if (resultados.next()) {
                codigo = resultados.getInt("codigo");
            }
        } catch (Exception e) {
            System.out.println("Error al obtener el código de usuario");
        }
        return codigo;
    }

    public Usuario obtenerDatosUsuario(int codigo) {

        abrirConexionBD();
        Usuario usuario = new Usuario();
        ResultSet resultados = null;
        try {
            String con;
            Statement s = conexionBD.createStatement();
            con = "SELECT * FROM usuarios WHERE codigo = '" + codigo + "'";
            resultados = s.executeQuery(con);

            if (resultados.next()) {

                usuario.setUsuario(resultados.getString("usuario"));
                usuario.setClave(resultados.getString("clave"));
                usuario.setNombre(resultados.getString("nombre"));
                usuario.setApellido(resultados.getString("apellidos"));
                usuario.setDireccion(resultados.getString("domicilio"));
                usuario.setLocalidad(resultados.getString("poblacion"));
                usuario.setProvincia(resultados.getString("provincia"));
                usuario.setCp(resultados.getString("cp"));
                usuario.setTelefono(resultados.getString("telefono"));
            }
        } catch (Exception e) {
            System.out.println("Error consultando el usuario....");
            System.out.print(e.getMessage());
        }
        return usuario;
    }

    public int getStock(int codigo) {
        abrirConexionBD();
        int stock = 0;
        ResultSet resultados = null;
        try {
            String con;
            Statement s = conexionBD.createStatement();
            con = "SELECT existencias FROM productos WHERE codigo = '" + codigo + "'";
            resultados = s.executeQuery(con);

            if (resultados.next()) {
                stock = resultados.getInt("existencias");
            }
        } catch (Exception e) {
            System.out.println("Error obteniendo el número de existencias....");
            System.out.print(e.getMessage());
        }
        return stock;
    }

    public void actualizarDatosUsuario(String user, String nombre, String apellidos, String telf, String direccion, String localidad, String provincia, String cp, HttpSession sesion) {
        abrirConexionBD();
        boolean estado = false;
        try {
            String con;
            con = "UPDATE usuarios SET nombre = '" + nombre + "', apellidos = '" + apellidos + "', domicilio = '" + direccion + "', poblacion = '" + localidad + "', provincia = '" + provincia + "', telefono = '" + telf + "', cp = '" + cp + "'   WHERE usuario = '" + user + "'";
            PreparedStatement actualizarUsuario = conexionBD.prepareStatement(con);
            if (actualizarUsuario.executeUpdate() == 1) {
                estado = true;
            } else {
                estado = false;
            }
        } catch (Exception e) {
            System.out.println("Error actualizando el usuario....");
            System.out.print(e.getMessage());
        }
    }

    public void actualizarPassword(String passw, String user) {
        abrirConexionBD();
        boolean estado = false;
        try {
            String con;
            con = "UPDATE usuarios SET clave = '" + passw + "' WHERE usuario = '" + user + "'";
            System.out.println(con);
            PreparedStatement s = conexionBD.prepareStatement(con);
            System.out.println("Cambio hecho");
            if (s.executeUpdate() == 1) {
                estado = true;
            } else {
                estado = false;
            }
        } catch (Exception e) {
            System.out.println("Error actualizando la contraseña....");
            System.out.print(e.getMessage());
        }
    }

    public boolean registrarPedido(int estado, Date fecha, float importe, int codigo) {
        boolean resultado = false;
        PreparedStatement s;
        try {
            String con;
            abrirConexionBD();
            con = "INSERT INTO pedidos (persona, fecha, importe, estado) values(?,?,?,?)";
            s = conexionBD.prepareStatement(con);
            s.setInt(1, codigo);
            s.setDate(2, fecha);
            s.setFloat(3, importe);
            s.setInt(4, estado);
            if (s.executeUpdate() == 1) {
                resultado = true;
            } else {
                resultado = false;
            }
        } catch (Exception e) {
            resultado = false;
            System.out.println(e.getMessage());
            System.out.println("Error introduciendo el pedido en la BB.DD....");
        }
        return resultado;
    }

    public int getCodigoPedido(int persona, Date fecha) {
        abrirConexionBD();
        ResultSet resultados;
        int codigo = 0;
        try {
            String con;
            Statement s = conexionBD.createStatement();
            con = "SELECT codigo FROM pedidos WHERE fecha='" + fecha + "' AND persona='"+persona+"' ORDER BY codigo DESC LIMIT 1";
            resultados = s.executeQuery(con);

            if (resultados.next()) {
                codigo = resultados.getInt("codigo");
            }
        } catch (Exception e) {
            System.out.println("Error al obtener el código del pedido");
        }
        return codigo;
    }

    public boolean registrarDetalle(int cPedido, int cProducto, int unidades, float precioUnitario) {
        boolean resultado = false;
        PreparedStatement s;
        try {
            String con;
            abrirConexionBD();
            System.out.println(cPedido);
            System.out.println(cProducto);
            System.out.println(unidades);
            System.out.println(precioUnitario);
            con = "INSERT INTO detalle (codigo_pedido, codigo_producto, unidades, precio_unitario) values(?,?,?,?)";
            s = conexionBD.prepareStatement(con);
            s.setInt(1, cPedido);
            s.setInt(2, cProducto);
            s.setInt(3, unidades);
            s.setFloat(4, precioUnitario);
            if (s.executeUpdate() == 1) {
                resultado = true;
            } else {
                resultado = false;
                System.out.println("Error de insercion");
            }
        } catch (Exception e) {
            resultado = false;
            System.out.println(e.getMessage());
            System.out.println("Error introduciendo el detalle en la BB.DD....");
        }
        return resultado;
    }
    
    public List<Pedido> obtenerPedido(int usuario) {

        abrirConexionBD();
        List<Pedido> pedidos = new ArrayList<>();
        ResultSet resultados = null;
        Pedido pedido = new Pedido();
        try {
            String con;
            Statement s = conexionBD.createStatement();
            con = "SELECT * FROM pedidos WHERE persona = '" + usuario + "'";
            resultados = s.executeQuery(con);

            while (resultados.next()) {
                pedido.setCodigo(resultados.getInt("codigo"));
                pedido.setEstado(resultados.getString("estado"));
                pedido.setPrecioTotal(resultados.getFloat("importe"));
                pedido.setFecha(resultados.getDate("fecha"));
                pedidos.add(pedido);
                pedido = new Pedido();
            }
        } catch (Exception e) {
            System.out.println("Error obteniendo el pedido....");
            System.out.print(e.getMessage());
        }
        return pedidos;
    }
    
    public boolean eliminarDetalle(int codigo){
        
        boolean resultado = false;
        abrirConexionBD();

        try{
            String con;
            Statement s = conexionBD.createStatement();
            con = "DELETE FROM detalle WHERE codigo_pedido = " + codigo;
            s.executeUpdate(con);
            con = "DELETE FROM pedidos WHERE codigo = " + codigo;
            s.executeUpdate(con);
        }catch (Exception e) {
            System.out.println("Error eliminando el pedido....");
            System.out.print(e.getMessage());
        }
        return resultado;
    }

}
