/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p2;

/**
 *
 * @author Joaquin Giner y Pablo Pons
 */

public class Usuario {
    private String usuario;
    private String nombre;
    private String apellido;
    private String telefono;
    private String localidad;
    private String direccion;
    private String provincia;
    private String clave;
    private String cp;

    public Usuario() {
    }

    public Usuario(String nombre, String apellido, String telefono, String localidad, String direccion, String provincia, String clave, String cp) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
        this.localidad = localidad;
        this.direccion = direccion;
        this.provincia = provincia;
        this.clave = clave;
        this.cp = cp;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }
    
    

    
}
